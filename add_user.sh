#! bin/bash
read -r line < data.txt
echo $line
username=$(echo $line | cut -d "|" -f1)
key=$(echo $line | cut -d "|" -f2)
password=$(echo $line | cut -d "|" -f3)
name=$(echo $line | cut -d "|" -f4)
echo $username "will be created with key " $key
echo "admin" | sudo adduser $username --disabled-password --gecos "$name, Room, Work, Home"
#echo $username:$password | sudo chpasswd
echo "admin" | sudo su $username
if test -e /home/$username/.ssh
then
    cd /home/$username/.ssh
    echo "admin" | sudo echo $key >> /home/$username/.ssh/authorized_keys
else
    echo "admin" | sudo mkdir /home/$username/.ssh
    echo "admin" | sudo chown superadmin /home/$username/.ssh
    touch /home/$username/.ssh/authorized_keys
    echo "admin" | sudo echo $key >> /home/$username/.ssh/authorized_keys
    echo "admin" | chmod 700 /home/$username/.ssh
    chmod 644 /home/$username/.ssh/authorized_keys
    echo "admin" | sudo chown $username /home/$username/.ssh/authorized_keys
    echo "admin" | sudo chown $username /home/$username/.ssh
fi