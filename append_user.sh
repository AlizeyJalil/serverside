#!/usr/bin/env bash

if ! test -e append_data.txt;
then
    echo "Append File not found!"
    exit
fi;
read -r line < append_data.txt
username=$(echo $line | cut -d "|" -f1)
key=$(echo $line | cut -d "|" -f2)
user_found=0
count=$(getent passwd $username | wc -l)
if test $count -eq 0;
then
    echo "User $username does not exist"
    exit
fi;
if test -e /home/$username/.ssh
then
    # Output redirected to null as key was being echoed. Beaware of bugs introuced due to this
     echo "$key" | sudo tee -a /home/$username/.ssh/authorized_keys > /dev/null
fi;