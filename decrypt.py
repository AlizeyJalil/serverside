from Crypto.Cipher import AES

key = '1234567891234567'
iv = '1234567891234567'

encryption_suite = AES.new(key, AES.MODE_CFB, iv)

def decryption(cipher_message):

    decrypted_message = encryption_suite.decrypt(cipher_message)
    return decrypted_message

def encryption(plain_message):

    enrypted_message = encryption_suite.encrypt(plain_message)
    return enrypted_message