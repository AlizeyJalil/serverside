#!/usr/bin/env bash


if test $# -ne 1
then
    echo "Please provide username to delete user"
    exit
fi;
echo admin | sudo deluser --remove-home $1