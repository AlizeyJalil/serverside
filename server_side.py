import pyrebase
import os
import decrypt

flag = 0
delete_flag = 0
config = {
    "apiKey": "AIzaSyDDsSxdM5rAUgSimd5EbSYAoyuaH3qdWV",
    "authDomain": "awesomeproj123.firebaseapp.com",
    "databaseURL": "https://awesomeproj123.firebaseio.com/",
    "storageBucket": "awesomeproj123.appspot.com",
    "serviceAccount": "awesomeproj123-firebase-adminsdk-hlndr-dd7b97537c.json"
}

# make changes to code according to the ips
# remove the returns
firebase = pyrebase.initialize_app(config)
db = firebase.database()

dev_ip = db.child("servers").get().val()['development']
qa_ip = db.child("servers").get().val()['qa']
prod_ip = db.child("servers").get().val()['production']
#def handle_patch(message):

def handle_put(path, data):
    if flag == 0:
        global flag
        flag = 1
        return
    else:
        if data != None:
            f = open("data.txt", "w+")
            #writing the info received from the db to the text file
            line_to_added = data['user_name'] + "|" + data['pub_key'] + "|" + data['password'] + "|" + data['name'] + "|" + data['sudo_prev']

            f.write(line_to_added)
            f.close()
            if 'qa' in data['server_name']:
                os.system("scp -P 2222 data.txt superadmin@" + qa_ip + ":/home/superadmin/")
                os.system("scp -P 2222 add_user.sh superadmin@"+ qa_ip +":/home/superadmin/")
                os.system("sh exec_adduser_at_server.sh " + qa_ip)

            if 'dev' in data['server_name']:
                os.system("scp -P 2222 data.txt superadmin@" + dev_ip + ":/home/superadmin/")
                os.system("scp -P 2222 add_user.sh superadmin@" + dev_ip + ":/home/superadmin/")
                os.system("sh exec_adduser_at_server.sh " + dev_ip)

            if 'prod' in data['server_name']:
                os.system("scp -P 2222 data.txt superadmin@" + prod_ip + ":/home/superadmin/")
                os.system("scp -P 2222 add_user.sh superadmin@" + prod_ip + ":/home/superadmin/")
                os.system("sh exec_adduser_at_server.sh " + prod_ip)


def user_handler(message):
    print(message["event"])  # put
    print(message["path"])  # /-K7yGTTEp7O549EzTYtI #gives the /username
    print(message["data"])  # {'title': 'Pyrebase', "body": "etc..."} #gives the data
    event = message['event']
    path = message['path']
    data = message['data']

    if event == 'put':
        handle_put(path, data)

    if event == 'patch':
        handle_patch(path, data)

def delete_handler(message):
    print(message)
    if delete_flag == 0:
        global delete_flag
        delete_flag = 1
        return
    else:
        print(message)
        data = message["data"]
        try:
            user_name = data["user_name"]# Try this, if caught TypeError, then the data was not deleted from portal
        except TypeError as err:
            print(err)
            return
        db.child("todelete").child(user_name).remove()
        if 'qa' in data['server_name']:
            os.system("scp -P 2222 del_user.sh superadmin@" + qa_ip + ":/home/superadmin/")
            os.system("sh exec_deluser_at_server.sh " + qa_ip + " " + user_name)

        if 'dev' in data['server_name']:
            os.system("scp -P 2222 del_user.sh superadmin@" + dev_ip + ":/home/superadmin/")
            os.system("sh exec_deluser_at_server.sh " + dev_ip + " " + user_name)

        if 'prod' in data['server_name']:
            os.system("scp -P 2222 del_user.sh superadmin@" + prod_ip + ":/home/superadmin/")
            os.system("sh exec_deluser_at_server.sh " + prod_ip + " " + user_name)


def handle_patch(path, data):
    #print(str(path) + " " + str(data))
    pathn = str(path).lstrip('/')
    #print(pathn)
    data = db.child("users").child(pathn).get().val()

    f = open("append_data.txt", "w+")
    # writing the info received from the db to the text file
    line_to_added = data['user_name'] + "|" + data['pub_key']

    f.write(line_to_added)
    f.close()

    if 'qa' in data['server_name']:
        os.system("scp -P 2222 append_data.txt superadmin@" + qa_ip + ":/home/superadmin/")
        os.system("scp -P 2222 append_user.sh superadmin@" + qa_ip + ":/home/superadmin/")
        os.system("sh exec_append_at_server.sh " + qa_ip)

    if 'dev' in data['server_name']:
        os.system("scp -P 2222 append_data.txt superadmin@" + dev_ip + ":/home/superadmin/")
        os.system("scp -P 2222 append_user.sh superadmin@" + dev_ip + ":/home/superadmin/")
        os.system("sh exec_append_at_server.sh " + dev_ip)

    if 'prod' in data['server_name']:
        os.system("scp -P 2222 append_data.txt superadmin@" + prod_ip + ":/home/superadmin/")
        os.system("scp -P 2222 append_user.sh superadmin@" + prod_ip + ":/home/superadmin/")
        os.system("sh exec_append_at_server.sh " + prod_ip)






my_stream = db.child("users").stream(user_handler)
delete_stream = db.child("todelete").stream(delete_handler)
#Note delete user works, just need to put some validations